import turtle as Logo

Logo.begin_fill()
Logo.fillcolor('blue')
for i in range(4):
    Logo.forward(90)
    Logo.right(90)
Logo.end_fill()

Logo.forward(90)
Logo.penup()
Logo.forward(10)
Logo.pendown()

Logo.begin_fill()
Logo.fillcolor('yellow')
for i in range(4):
    Logo.forward(90)
    Logo.right(90)
Logo.end_fill()

Logo.left(90)
Logo.penup()
Logo.forward(100)
Logo.pendown()
Logo.right(90)

Logo.begin_fill()
Logo.fillcolor('green')
for i in range(4):
    Logo.forward(90)
    Logo.right(90)
Logo.end_fill()

Logo.left(180)
Logo.penup()
Logo.forward(10)
Logo.pendown()
Logo.forward(90)
Logo.left(90)

Logo.begin_fill()
Logo.fillcolor('red')
for i in range(4):
    Logo.forward(90)
    Logo.left(90)
Logo.end_fill()

Logo.penup()
Logo.forward(240)
Logo.pendown()
Logo.write("Microsoft", font=("Pacifico", 24))


