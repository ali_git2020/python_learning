import turtle as Scenery

Scenery.begin_fill() 
Scenery.fillcolor('grey')
Scenery.backward(500)
Scenery.forward(1000)
Scenery.right(90)
Scenery.forward(150)
Scenery.right(90)
Scenery.forward(1000)
Scenery.right(90)
Scenery.forward(150)
Scenery.end_fill()
Scenery.backward(75)

Scenery.right(90)
Scenery.forward(90)
Scenery.penup()
Scenery.forward(90)
Scenery.pendown()
Scenery.forward(90)
Scenery.penup()
Scenery.forward(90)
Scenery.pendown()
Scenery.forward(90)
Scenery.penup()
Scenery.forward(90)
Scenery.pendown()
Scenery.forward(90)
Scenery.penup()
Scenery.forward(90)
Scenery.pendown()
Scenery.forward(90)
Scenery.penup()
Scenery.forward(90)
Scenery.pendown()
Scenery.forward(90)
Scenery.penup()
Scenery.forward(10)
Scenery.left(90)
Scenery.pendown()
Scenery.forward(75)

Scenery.left(90)
Scenery.forward(820)
Scenery.right(90)

Scenery.begin_fill()      
Scenery.color("black")
Scenery.forward(170)
Scenery.right(90)
Scenery.forward(350)
Scenery.right(90)
Scenery.forward(85)
Scenery.left(75)
Scenery.forward(75)
Scenery.right(75)
Scenery.forward(30)
Scenery.right(60)
Scenery.forward(5)
Scenery.fillcolor('yellow')
Scenery.circle(7)
Scenery.forward(10)
Scenery.left(60)
Scenery.forward(10)
Scenery.left(90)
Scenery.forward(15)
Scenery.right(90)
Scenery.forward(20)
Scenery.right(90)
Scenery.forward(350)
Scenery.end_fill()

Scenery.begin_fill() 
Scenery.fillcolor('black')
Scenery.circle(25)
Scenery.backward(200)
Scenery.circle(25)
Scenery.end_fill()
Scenery.begin_fill() 
Scenery.fillcolor('blue')
Scenery.right(90)
Scenery.forward(90)
Scenery.right(90)
Scenery.forward(40)
Scenery.right(90)
Scenery.forward(90)
Scenery.end_fill()

Scenery.right(90)
Scenery.forward(40)
Scenery.right(90)
Scenery.penup()
Scenery.forward(100)
Scenery.begin_fill() 
Scenery.fillcolor('blue')
Scenery.left(90)
Scenery.forward(200)
Scenery.pendown()
Scenery.right(90)
Scenery.forward(40)
Scenery.right(90)
Scenery.forward(150)
Scenery.right(90)
Scenery.forward(40)
Scenery.right(90)
Scenery.forward(150)
Scenery.end_fill()
Scenery.penup()
Scenery.left(90)
Scenery.forward(100)
Scenery.left(90)
Scenery.forward(500)

Scenery.pendown()
Scenery.left(90)
Scenery.begin_fill() 
Scenery.fillcolor('brown')
Scenery.forward(170)
Scenery.right(90)
Scenery.forward(40)
Scenery.right(90)
Scenery.forward(170)
Scenery.right(90)
Scenery.forward(40)
Scenery.end_fill()

Scenery.begin_fill() 
Scenery.fillcolor('green')
Scenery.right(90)
Scenery.forward(170)
Scenery.left(90)
Scenery.forward(120)
Scenery.right(135)
Scenery.forward(200)
Scenery.right(90)
Scenery.forward(200)
Scenery.right(135)
Scenery.forward(160)
Scenery.end_fill()

Scenery.penup()
Scenery.forward(800)
Scenery.right(90)
Scenery.forward(100)
Scenery.pendown()
Scenery.begin_fill() 
Scenery.fillcolor('orange')
Scenery.circle(35)
Scenery.end_fill()








