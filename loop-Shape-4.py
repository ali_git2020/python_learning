import turtle as Loop
colors = ['red','blue','green','yellow','orange','brown','purple']
Loop.backward(400)
for i in range(2):
    Loop.circle(10)
    Loop.forward(400)
    Loop.right(90)
    Loop.forward(20)
    Loop.right(90)
Loop.forward(100)

for i in range(6,0,-1):

    Loop.color(colors[i])
    Loop.begin_fill()
    Loop.circle(i*20)
    Loop.circle(i*-20)
    Loop.end_fill()
   # Loop.right(45)

